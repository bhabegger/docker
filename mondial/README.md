# Mondial

Docker image of the postgresql version of the Mondial Database

## Usage
```
docker pull bhabegger/mondial
docker run  -p 5432:5432 --name mondial bhabegger/mondial
```

The first time it runs it will be slow as it will create and populate the databases. The subsequence starts will be quick

